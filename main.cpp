#include <QCoreApplication>
#include <QtDBus/QtDBus>
#include <QtDBus/QDBusConnection>
#include "dalek.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    Dalek *dalek = new Dalek();
    new DaemonInterfaceAdaptor(dalek);
    QDBusConnection connection = QDBusConnection::sessionBus();
    connection.registerObject("/API", dalek);
    connection.registerService("im.skaro.Daemon");

    return app.exec();
}
