#ifndef __LIBSKARO_H_
#define __LIBSKARO_H_

#ifdef __cplusplus
extern "C" {
#endif

    // Регистрация/Авторизация
    void skaro_export_keys(const char* login, const char* public_key, const char* private_key);
    void skaro_on_authorize(const char* status, const char* info, const char* session_id, const char* data);
    void skaro_on_register_type(const char* status, const char* info, const char* register_type);
    void skaro_on_register(const char* status, const char* info);
    void skaro_on_session_restore(const char* status, const char* info, const char* session_id, const char* data);

    // Работа с контактами
    void skaro_on_get_contacts(const char* status, const char* info, const char* data);
    void skaro_on_add_contact(const char* status, const char* info, const char* data);
    void skaro_on_del_contact(const char* status, const char* info, const char* login);
    void skaro_on_search_contacts(const char* status, const char* info, const char* data);

    // Работа с пользователями
    void skaro_on_get_user_data(const char* status, const char* info, const char* data);
    void skaro_on_changed_user_data(const char* login, const char* data);

    // Работа с сообщениями
    void skaro_on_get_history(const char* status, const char* info, const char* login, const char* data);
    void skaro_on_send_message(const char* status, const char* info, int mid, const char* date, const char* login, const char* marker);
    void skaro_on_received_message(int mid, const char* date, const char* from, const char* body);

    // Работа со своими данными
    void skaro_on_update_my_data(const char* status, const char* info, const char* records);

    // Прочее
    void skaro_on_error(const char* status, const char* info);
    void skaro_on_info(const char* status, const char* info);



    typedef void * handle;
    extern handle skaro_connect(const char*);
    void skaro_disconnect(handle);
    void skaro_authorize(handle, const char*, const char*, const char*);
    void skaro_register_type(handle);
    void skaro_register(handle, const char*, const char*, const char*);
    void skaro_session_restore(handle, const char*);

    void skaro_get_contacts(handle);
    void skaro_add_contact(handle, const char*);
    void skaro_del_contact(handle, const char*);
    void skaro_search_contacts(handle, const char*);

    void skaro_get_user_data(handle, const char*);

    void skaro_get_history(handle, const char*);
    void skaro_send_message(handle, const char*, const char*, bool, const char*);

    void skaro_update_my_data(handle, const char*);

#ifdef __cplusplus
}
#endif

#endif
