#include <stdio.h>
#include <stdlib.h>

#include <QDebug>
#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>
#include <QDir>
#include <QSettings>
#include <QJsonDocument>

#include "dalek.h"

const char* glob_session_id;  // хранить в файле
Dalek *d;

// Регистрация/Авторизация

void skaro_export_keys(const char* login, const char* public_key, const char* private_key) {
    QString qlogin = QString::fromUtf8(login);

    QDir qdir;
    QString home = qdir.homePath();

    qdir.mkdir(home+"/.config/skaro/"+qlogin);

    QFile file_private(home+"/.config/skaro/"+qlogin+"/id_rsa");
    if(file_private.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        file_private.write(private_key);
        file_private.close();
    }

    QFile file_public(home+"/.config/skaro/"+qlogin+"/id_rsa.pub");
    if(file_public.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        file_public.write(public_key);
        file_public.close();
    }
}

void skaro_on_authorize(const char* status, const char* info, const char* session_id, const char* data) {
    QSettings settings("Exarh Team", "skaro");
    settings.setValue("session_id", session_id);
    d->user_data = data;
    d->emit onAuthorize(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(data));
}

void skaro_on_register_type(const char* status, const char* info, const char* register_type) {
    d->emit onConnect(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(register_type));
}

void skaro_on_register(const char* status, const char* info) {
    d->emit onRegister(QString::fromUtf8(status), QString::fromUtf8(info));
}

void skaro_on_session_restore(const char* status, const char* info, const char* session_id, const char* data) {
    QSettings settings("Exarh Team", "skaro");

    if (QString::fromUtf8(status) == "200 OK") {
        settings.setValue("session_id", session_id);
        d->user_data = data;
        d->emit onAuthorize(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(data));
    } else {
        settings.setValue("session_id", "");

        QString private_key = "";
        QFile file("/home/modos189/.config/skaro/"+d->login+"/id_rsa");
        if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
        {
            private_key = file.readAll();
            file.close();
        }

        skaro_authorize(
                    d->x,
                    d->login.toStdString().c_str(),
                    private_key.toStdString().c_str(),
                    d->password.toStdString().c_str());

    }
}

// Работа с контактами

void skaro_on_get_contacts(const char* status, const char* info, const char* data) {
    d->emit onGetContacts(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(data));
}

void skaro_on_add_contact(const char* status, const char* info, const char* data) {
    d->emit onAddContact(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(data));
}

void skaro_on_del_contact(const char* status, const char* info, const char* login) {
    d->emit onDelContact(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(login));
}

void skaro_on_search_contacts(const char* status, const char* info, const char* data) {
    d->emit onSearchContacts(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(data));
}

// Работа с пользователями

void skaro_on_get_user_data(const char* status, const char* info, const char* data) {
    d->emit onGetUserData(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(data));
}

void skaro_on_changed_user_data(const char* login, const char* data) {
    d->emit onChangedUserData(QString::fromUtf8(login), QString::fromUtf8(data));
}

// Работа с сообщениями

void skaro_on_get_history(const char* status, const char* info, const char* login, const char* data) {
    d->emit onGetHistory(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(login), QString::fromUtf8(data));
}

void skaro_on_send_message(const char* status, const char* info, int mid, const char* date, const char* login, const char* marker) {
    d->emit onSendMessage(QString::fromUtf8(status), QString::fromUtf8(info), mid, QString::fromUtf8(date), QString::fromUtf8(login), QString::fromUtf8(marker));
}

void skaro_on_received_message(int mid, const char* date, const char* from, const char* body) {
    d->emit onReceivedMessage(mid, QString::fromUtf8(date), QString::fromUtf8(from), QString::fromUtf8(body));
}

// Работа со своими данными

void skaro_on_update_my_data(const char* status, const char* info, const char* records) {
    d->emit onUpdateMyData(QString::fromUtf8(status), QString::fromUtf8(info), QString::fromUtf8(records));
}

// Прочее

void skaro_on_error(const char* status, const char* info) {
    d->emit onError(QString::fromUtf8(status), QString::fromUtf8(info));
}

void skaro_on_info(const char* status, const char* info) {
    d->emit onInfo(QString::fromUtf8(status), QString::fromUtf8(info));
}



Dalek::Dalek()
{
    d = this;
}

void Dalek::connect(QString domain)
{
    QStringList::Iterator it;

    if (this->x == NULL) {
        QString url = "ws://"+domain+":8000/skaro_client/";
        this->x = skaro_connect(url.toStdString().c_str());
    } else {
        if (this->user_data != NULL) {
            emit onAuthorize("200 OK", "", this->user_data);
            return;
        }
    }
    //emit onConnect(42);

    QString path = QDir::homePath()+"/.config/skaro/";
    QDir dir(path);
    QStringList accounts;

    QStringList dirs = dir.entryList(QDir::Dirs);
    it = dirs.begin();
    while (it != dirs.end()) {
        if (*it != "." && *it != "..") {
            if (
                    QFileInfo(path + "/" + *it, "id_rsa").exists() and
                    QFileInfo(path + "/" + *it, "id_rsa.pub").exists()
            ) {

                QString current = *it;
                accounts << current;

            }
        }
        ++it;
    }

    QJsonArray object = QJsonArray::fromStringList(accounts);

    QJsonDocument document;
    document.setArray(object);

    d->emit onDaemonGetAccounts(document.toJson());
    skaro_register_type(this->x);
}

void Dalek::disconnect()
{
    skaro_disconnect(x);
}

void Dalek::authorize(QString login, QString password)
{
    QSettings settings("Exarh Team", "skaro");
    QString session_id = settings.value("session_id").toString();

    this->login = login;
    this->password = password;

    if (session_id == "") {
        this->__startAuthorize();
    } else {
        skaro_session_restore(this->x, session_id.toStdString().c_str());
    }
}

void Dalek::__startAuthorize()
{
    QString private_key = "";
    QFile file("/home/modos189/.config/skaro/"+this->login+"/id_rsa");
    if ((file.exists())&&(file.open(QIODevice::ReadOnly)))
    {
        private_key = file.readAll();
        file.close();
    }

    skaro_authorize(
                this->x,
                this->login.toStdString().c_str(),
                private_key.toStdString().c_str(),
                this->password.toStdString().c_str());
}

void Dalek::startRegister(QString email, QString login, QString password)
{
    skaro_register(this->x, email.toStdString().c_str(), login.toStdString().c_str(), password.toStdString().c_str());
}



void Dalek::getContacts()
{
    skaro_get_contacts(this->x);
}

void Dalek::addContact(QString login)
{
    skaro_add_contact(this->x, login.toStdString().c_str());
}

void Dalek::delContact(QString login)
{
    skaro_del_contact(this->x, login.toStdString().c_str());
}

void Dalek::searchContacts(QString query)
{
    skaro_search_contacts(this->x, query.toStdString().c_str());
}



void Dalek::getUserData(QString login)
{
    skaro_get_user_data(this->x, login.toStdString().c_str());
}



void Dalek::getHistory(QString login)
{
    skaro_get_history(this->x, login.toStdString().c_str());
}

QString Dalek::sendMessage(QString to, QString body, bool is_conference)
{
    int max_num = 999999;
    QString marker = QString::number(qrand() % max_num);
    skaro_send_message(this->x, to.toStdString().c_str(), body.toStdString().c_str(), is_conference, marker.toStdString().c_str());
    return marker;
}



void Dalek::updateMyData(QString records)
{
    skaro_update_my_data(this->x, records.toStdString().c_str());
}
