#ifndef DALEK_H
#define DALEK_H

#include <QtCore/QObject>
#include "skaro_adaptor.h"
#include "skaro.h"

class Dalek: public QObject
{
    Q_OBJECT




public:
    handle x;
    QString login;
    QString password;
    QString user_data;

    Dalek();
    void __startAuthorize();

signals:
    // Регистрация/Авторизация
    void onConnect(QString status, QString info, QString register_type);
    void onDaemonGetAccounts(QString accounts);
    void onAuthorize(QString status, QString info, QString data);
    void onRegister(QString status, QString info);
    // Работа с контактами
    void onGetContacts(QString status, QString info, QString data);
    void onAddContact(QString status, QString info, QString data);
    void onDelContact(QString status, QString info, QString login);
    void onSearchContacts(QString status, QString info, QString data);
    // Работа с пользователями
    void onGetUserData(QString status, QString info, QString data);
    void onChangedUserData(QString login, QString data);
    // Работа с сообщениями
    void onGetHistory(QString status, QString info, QString login, QString data);
    void onSendMessage(QString status, QString info, int mid, QString date, QString login, QString marker);
    void onReceivedMessage(int mid, QString date, QString from, QString body);
    // Работа со своими данными
    void onUpdateMyData(QString status, QString info, QString records);
    // Прочее
    void onError(QString status, QString info);
    void onInfo(QString status, QString info);



public slots:
    void connect(QString domain);
    void disconnect();
    void authorize(QString login, QString password);
    void startRegister(QString email, QString login, QString password);

    void getContacts();
    void addContact(QString login);
    void delContact(QString login);
    void searchContacts(QString query);

    void getUserData(QString login);

    void getHistory(QString login);
    QString sendMessage(QString to, QString body, bool is_conference);

    void updateMyData(QString records);
};

#endif
